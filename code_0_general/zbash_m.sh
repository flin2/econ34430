#!/bin/bash

# bash zbash_m.sh "" "" ""

script=$1
log=$2
param=$3

export script
export param

# If ${log} is empty, use default log file
if [ -z "${log}" ]
then
    log="log/${script%.m}.mout"
fi

# Run script
/Applications/MATLAB_R2020a.app/bin/matlab -sd ${PWD} -batch "${script%.m}" -logfile "${log}"

# End of bash script