#!/bin/bash

# bash zbash_r.sh "" "" ""

script=$1
log=$2
param=$3

export script

# If ${log} is empty, use default log file
if [ -z "${log}" ]
then
    log="log/${script%.R}.Rout"
fi

# run Rscript
Rscript  ${script} ${param} > ${log} 2>&1

# End of bash script