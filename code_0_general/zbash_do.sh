#!/bin/bash

# bash zbash_do.sh "" "" ""

script=$1
log=$2
param=$3
sys_os=mac
dir_proj="~/Dropbox/0_project/0_project_template/"

export script sys_os dir_proj

# If ${log} is empty, use default log file
if [ -z "${log}" ]
then
    log="log/${script%.do}.log"
fi

# Run script
stata-mp -b ${script} ${param}

mv ${script%.do}.log ${log}

# End of bash script