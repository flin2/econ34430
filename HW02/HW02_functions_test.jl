#=
ECON 34430 Topics in Labor Markets: Earnings and Employment Assignment 2
Feng Lin
cd ~/Dropbox/Chicago/ECON34430_Lamadon/HW02/
bash zbash_jl.sh "HW02_data.jl" "" ""
=#


#-------------------------------------------------------------------------------
# Parameters

if sys_dev == true
    # Number of types of firms and workers
    nk = 30
    nl = 10
    nt = 15
    
    # Offer probability
    λ = 0.1
    
    # Death probability
    ρ = 0.1
    
    # (ψ_k,V_k,f_k) ~ N, where f_j means probability of meeting firm type k
    μ_firm = [0,0,0]
    Σ_firm = [1 0.75 0.6; 0.75 1 0.5; 0.6 0.5 1]
    
    # α ~ N
    μ_worker = 0
    σ_worker = 1
    
    # Number of individuals
    ni = 10000
    # Number of firm per type
    nj_per_k = 15
    
    # σ_wage
    σ_wage = 1
end


if sys_dev == true
    ψ,V,f,f_raw = f_dgp_firmtype(μ_firm,Σ_firm,p_nk)
    α = f_dgp_workertype(0,1,p_nl)
    G,H = f_dgp_dist(p_nk,p_nl,ψ,α)
end

if sys_dev == true
    ψ,α,ll,kk,spellcount=f_dgp_panel(nk,nl,nt,ni,λ,ρ,μ_firm,Σ_firm,μ_worker,σ_worker;p_seed=6063700)
end


if sys_dev == true
    jj = f_dgp_firmid(kk,spellcount,nj_per_k;p_seed=6063705)
end

if sys_dev == true
    df_raw = f_dgp_df(ll,jj,kk,α,ψ,spellcount,σ_wage)
end

if sys_dev == true
    df_raw = f_dgp(nk,nl,nt,ni,nj_per_k,λ,ρ,μ_firm,Σ_firm,μ_worker,σ_worker,σ_wage)
end

if sys_dev == true
    df_raw = @subset(df_raw,:k.>0)
    G,df_raw = f_connected_firms(;df_in=df_raw)
end

hp_dgp = struct_dgp(;μ_ψ=0,μ_V=0,μ_f=0,
        σ_ψ=1,σ_V=1,σ_f=1,σ_ψV=0.75,σ_ψf=0.6,σ_Vf=0.5,
        μ_worker=0,σ_worker=1,μ_wage=0,σ_wage=1,
        λ=0.1,ρ=0.1)
hp_dgp.μ_firm

hp_panel = struct_panel(30,10,10,10000,15)