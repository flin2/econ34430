# ECON 34430 Topics in Labor Markets: Earnings and Employment Assignment 2
# Feng Lin
# cd ~/Dropbox/Chicago/ECON34430_Lamadon/HW02/
# bash zbash_jl.sh "HW02_functions.jl" "" ""

@show ARGS

# Whether current run is for development
if ARGS == []
    sys_dev = false
else
    sys_dev = (ARGS[1]=="true")
end

if ! @isdefined dir_proj
    dir_proj = "/Users/flin/Dropbox/Chicago/ECON34430_Lamadon/HW02/" # Change to the directory where the zip file is unzipped
end
cd(dir_proj)

dir_data = "./data/"
dir_out = "./output/"

using Pkg,Dates,RCall
if false # When running the code for the first time, change to true
    Pkg.add(["Random","Distributions","StatsBase"])
    Pkg.add(["LinearAlgebra","Optim","BlackBoxOptim","NLsove"])
    Pkg.add(["DataFrames","DataFramesMeta","Chain","ShiftedArrays","CSV","StatFiles"])
    Pkg.add(["Plots","Gadfly","Compose","Cairo","Fontconfig"])
    Pkg.add(["DelimitedFiles"])
    Pkg.add(["FixedEffectModels","Vcov","RegressionTables"])
    Pkg.add(["LightGraphs"])
end
# Pkg.instantiate()

using Random,Distributions,StatsBase
using LinearAlgebra,Optim,BlackBoxOptim,NLsolve
using DataFrames,DataFramesMeta,Chain,ShiftedArrays,CSV,StatFiles
using Plots,Gadfly,Compose,Cairo,Fontconfig
using DelimitedFiles
using FixedEffectModels,Vcov,RegressionTables
using LightGraphs

# println(string("Started at ",Dates.format(now(), "yyyy-mm-dd HH:MM:SS\n")))

#===============================================================================
Structs
=#

# struct with info about the panel
mutable struct struct_panel
    nk::Int64 # Number of types of firms
    nl::Int64 # Number of active workers in each period
    nt::Int64 # Number of periods
    ni::Int64 # Number of individuals
    nj_per_k::Int64 # Number of firm per type
end

# nk,nl,nt,ni,nj_per_k = hp_panel.nk,hp_panel.nl,hp_panel.nt,hp_panel.ni,hp_panel.nj_per_k

# struct with values related to DGP
mutable struct struct_dgp
    # (ψ_k,V_k,f_k) ~ N, where f_j means probability of meeting firm type k
    μ_firm::Vector{Float64}
    Σ_firm::Array{Float64, 2}
    
    # Distribution of α
    μ_worker::Float64
    σ_worker::Float64
    
    # Distribution of wage error term
    μ_wage::Float64
    σ_wage::Float64

    # Offer probability
    λ::Float64
    
    # Exit probability
    ρ::Float64
end

# Function that initiate hp_dgp
function struct_dgp(;μ_ψ::Real,μ_V::Real,μ_f::Real,
    σ_ψ::Real,σ_V::Real,σ_f::Real,σ_ψV::Real,σ_ψf::Real,σ_Vf::Real,
    μ_worker::Real,σ_worker::Real,μ_wage::Real,σ_wage::Real,
    λ::Real,ρ::Real)

    μ_firm = convert.(Float64,[μ_ψ,μ_V,μ_f])
    Σ_firm = convert.(Float64,[σ_ψ σ_ψV σ_ψf; σ_ψV σ_V σ_Vf; σ_ψf σ_Vf σ_f])
    return struct_dgp(μ_firm,Σ_firm,μ_worker,σ_worker,μ_wage,σ_wage,λ,ρ)
end

#===============================================================================
DGP
=#

#-------------------------------------------------------------------------------
# Characteristics by firm types and worker types

#---------------------------------------
# Firm

function f_dgp_firmtype(hp_panel::struct_panel,hp_dgp::struct_dgp;p_seed::Int64=6063701)
    p_seed != -1 ? Random.seed!(p_seed) : nothing

    # Draw firm characteristics
    mfirm = rand(MvNormal(hp_dgp.μ_firm,hp_dgp.Σ_firm),hp_panel.nk)'
    mfirm = sortslices(mfirm,dims=1,by=x->x[1],rev=false)
    ψ,V,f_raw = mfirm[:,1],mfirm[:,2],mfirm[:,3]
    
    # Normalize f to sum to 1 (offer shares)
    f = exp.(f_raw) ./ sum(exp.(f_raw))

    return ψ,V,f,f_raw
end

#---------------------------------------
# Worker

function f_dgp_workertype(hp_panel::struct_panel,hp_dgp::struct_dgp;p_seed::Int64=6063702)

    p_seed != -1 ? Random.seed!(p_seed) : nothing

    # Draw worker characteristics
    α = sort(rand(Normal(hp_dgp.μ_worker,hp_dgp.σ_worker),hp_panel.nl))

    return α
end

#---------------------------------------
# Initial Distribution (Stationary with some sorting)
#   We will use H to initiate t=1 and draw replacement workers

function f_dgp_dist(hp_panel::struct_panel,ψ,α,csort::Real=0.5,cnetw::Real=0.2,csig::Real=0.5)

    nk = hp_panel.nk
    nl = hp_panel.nl
    
    # Let's create type-specific transition matrices
    # We are going to use joint normals centered on different values
    # G = Probability of l moving from type k to k'
    #   cnetw characterizes how easy to move between two types of firms
    #   csort characterizes sorting between firms and workers (if ψ and csort * α are more similar, their tend to be together more)
    G = zeros(nl, nk, nk)
    for l in 1:nl, k in 1:nk
        G[l, k, :] = pdf( Normal(0, csig), ψ .- cnetw * ψ[k] .- csort * α[l])
        G[l, k, :] = G[l, k, :] ./ sum(G[l, k, :])
    end

    # We then solve for the stationary distribution over psis for each alpha value
    # We apply a crude fixed point approach
    # H = Stationary distribution
    H = ones(nl, nk) ./ nk
    for l in 1:nl
        M = transpose(G[l, :, :])
        for i in 1:100
            H[l, :] = M * H[l, :]
        end
    end

    return G,H
end

#-------------------------------------------------------------------------------
# Generate a panel based on decision rule

#---------------------------------------
# Function that returns initialized matrices
function f_dgp_initdist(nk,nl,ni,nt,H;p_seed::Int64=6063703)
    
    p_seed != -1 ? Random.seed!(p_seed) : nothing

    ll = zeros(Int64, ni, nt) # Worker type
    kk = zeros(Int64, ni, nt) # Firm type
    kko = zeros(Int64, ni, nt) # Firm type that offers job
    spellcount = zeros(Int64, ni, nt) # Employment spell
    ι = reshape(rand(Logistic(),ni*nt), ni, nt) # Idiosyncrtic shocks; if we add it on one side, use Logistic(); if two sides, use Gumbel()

    # Inital distribution drawn from H
    for i = 1:ni
        # We draw the worker type
        l = rand(1:nl)
        ll[i,:] .= l
        # We draw firm allocation from stationary distribution
        kk[i,1] = sample(1:nk, Weights(H[l, :]))
    end

    return ll,kk,kko,spellcount,ι
end

#=--------------------------------------
Function that generates a panel
Codes for kk:
    -1 = Worker has exited
    -2 = Worker has not entered
    0 (kko) = Worker does not receive an offer
=#

function f_dgp_panel(hp_panel::struct_panel,hp_dgp::struct_dgp;p_seed::Int64=6063700,p_quiet::Bool=true);
    
    nk,nl,nt,ni,nj_per_k = hp_panel.nk,hp_panel.nl,hp_panel.nt,hp_panel.ni,hp_panel.nj_per_k
    λ,ρ = hp_dgp.λ,hp_dgp.ρ

    ψ,V,f,f_raw = f_dgp_firmtype(hp_panel,hp_dgp;p_seed=p_seed+1)
    α = f_dgp_workertype(hp_panel,hp_dgp;p_seed=p_seed+2)
    G,H = f_dgp_dist(hp_panel,ψ,α)

    # We generate larger matrices to accomodate potential replacements
    ll,kk,kko,spellcount,ι = f_dgp_initdist(nk,nl,3*ni,nt,H;p_seed=p_seed+3)

    p_seed != -1 ? Random.seed!(p_seed+4) : nothing

    # Because number of workers will change by t, we will first loop over t, than i
    ll_alive = [1:ni;] # Workers that are in market
    ll_death = [] # Workers that have exited
    ni_max = ni # Max worker ID
    # Function to sample k x[1] = nk; x[2] = f; x[3] = k_l1
    if nj_per_k > 1
        f_dgp_ksample = x -> sample(1:x[1], Weights(x[2]))
    else 
        f_dgp_ksample = x -> sample(collect(1:x[1])[1:nk .!= k], Weights(x[2][1:nk .!= k]))
    end

    for t = 2:nt
        ll_born = [] # Workers born in this period
        for i = ll_alive
            #---------------------------
            # Whether the worker die
            if rand() < ρ # The worker dies
                p_quiet == false ? println("Exit") : nothing

                kk[i,t:end] .= -1
                kko[i,t:end] .= -1
                spellcount[i,t:end] .= -1
                ll_death = vcat(ll_death,i)
                
                # Check if more rows need to be generated
                if ni_max == size(ll)[1]
                    ll_new,kk_new,kko_new,spellcount_new,ι_new = f_dgp_initdist(nk,nl,ni,nt,H;p_seed=p_seed+t)
                    ll = vcat(ll,ll_new)
                    kk = vcat(kk,kk_new)
                    spellcount = vcat(spellcount,spellcount_new)
                    ι = vcat(ι,ι_new)
                end
                ni_max = ni_max + 1
                ll_born = vcat(ll_born,ni_max)

                # Deal with rows for the new worker
                kk[ni_max,t] = kk[ni_max,1]
                kk[ni_max,1:t-1] .= -2
                kko[ni_max,1:t-1] .= -2
                spellcount[ni_max,1:t-1] .= -2

                continue # End current loop
            end

            #---------------------------
            # Whether the worker receive an offer
            if rand() > λ # The worker does not receive an offer
                p_quiet == false ? println("Offer") : nothing

                kk[i,t] = kk[i,t-1]
                spellcount[i,t] = spellcount[i,t-1]
                continue # End current loop
            end

            #---------------------------
            # Where the worker receive the offer
            k_offer = f_dgp_ksample([nk,f,kk[i,t-1]])
            kko[i,t] = k_offer

            #---------------------------
            # Whether the worker move
            if V[kk[i,t-1]] < V[k_offer] + ι[i,t] # Move
                p_quiet == false ? println("Move") : nothing
                kk[i,t] = k_offer
                spellcount[i,t] = spellcount[i,t-1] + 1
            else # Not move
                p_quiet == false ? println("Stay") : nothing
                kk[i,t] = kk[i,t-1]
                spellcount[i,t] = spellcount[i,t-1]
            end

        end

        # Add new born to the list of alive
        ll_alive = vcat([il for il in ll_alive if il ∉ ll_death],ll_born)
    end

    return ψ,V,f,α,ll[1:ni_max,:],kk[1:ni_max,:],kko[1:ni_max,:],spellcount[1:ni_max,:]
end

#-------------------------------------------------------------------------------
# Generate firm ID

# Function that assigns firm ID to kk
function f_dgp_firmid(kk,kko,spellcount,nj_per_k;p_seed::Int64=6063700)

    # Function that draws firm ID by type
    draw_firm_from_type(k,nj_per_k) = sample(1:nj_per_k) + (k - 1) * nj_per_k
    
    p_seed != -1 ? Random.seed!(p_seed) : nothing

    ni,nt = size(kk)
    jj = zeros(Int64, ni, nt) # Firm identifiers
    
    for i in 1:ni
        
        #-------------------------------
        # Period 1
        t = 1
        
        # extract firm type
        k = kk[i,t]
        
        # We draw the firm (one of firms_per_type in given group)
        jj[i,t] = k>0 ? draw_firm_from_type(k,nj_per_k) : 0
        
        #-------------------------------
        for t in 2:nt
            k = kk[i,t]
            if k<=0
                jj[i,t] = 0
                continue
            end

            if spellcount[i,t] == spellcount[i,t-1]
                # We keep the firm the same
                jj[i,t] = jj[i,t-1]
            else
                # We draw a new firm
                new_j = draw_firm_from_type(k,nj_per_k)
                # Make sure the new firm is actually new, if nj_per_k > 1
                while new_j == jj[i,t-1]
                    new_j = draw_firm_from_type(k,nj_per_k)
                end

                jj[i,t] = new_j
            end
        end
    end

    # Make sure firm ids are contiguous
    jj_nonzero = [ijj for ijj in unique(jj) if ijj > 0]
    contiguous_ids = Dict( jj_nonzero .=> 1:length(jj_nonzero) )
    contiguous_ids[0] = 0;
    jj .= getindex.(Ref(contiguous_ids),jj);

    return jj
end # j=0 not a real firm

#-------------------------------------------------------------------------------
# Generate final dataframe

function f_dgp_df(ll,jj,kk,α,ψ,V,f,spellcount,hp_dgp::struct_dgp;p_seed::Int64=6063700)
    
    ni,nt = size(ll)

    ii = repeat(1:ni,1,nt)
    tt = repeat((1:nt)',ni,1)
    df = DataFrame(i=ii[:], j=jj[:], l=ll[:], k=kk[:], t=tt[:], spell=spellcount[:]);
    @transform!(df, @byrow :j = :j.==0 ? missing : :j)
    # Be careful, now kk includes -1 marking period after exit, and -2 marking period before entry

    # Add AKM wage
    p_seed != -1 ? Random.seed!(p_seed) : nothing
    @transform!(df,:ε_wage=rand(Normal(hp_dgp.μ_wage,hp_dgp.σ_wage), size(df)[1]))
    df = @eachrow df begin
        @newcol :α::Vector{Float64}
        @newcol :ψ::Vector{Union{Float64,Missing}}
        @newcol :V::Vector{Union{Float64,Missing}}
        @newcol :f::Vector{Union{Float64,Missing}}
        @newcol :lw::Vector{Union{Float64,Missing}}
        :α = α[:l]
        if :k > 0
            :ψ = ψ[:k]
            :V = V[:k]
            :f = f[:k]
            :lw = :α + :ψ + hp_dgp.σ_wage * :ε_wage
        else 
            :ψ = missing
            :V = missing
            :f = missing
            :lw = missing
        end
    end
    
    # Mark Movers
    df = @chain df begin
        @orderby(:i,:t)
        groupby([:i])
        @transform(:j_l1 = lag(:j,1),:k_l1 = lag(:k,1),:j_f1 = lead(:j,1),:k_f1 = lead(:k,1))

        @transform(@byrow :j_l1 = (:j_l1.===0) ? missing : :j_l1) # If not real move, set j_l1 to missing
        @transform(@byrow :imov = ( (:t.!= 1) .& (:k.>0).&(:k_l1.>0) .& (:j.!==:j_l1) ) ? 1 : 0) # Mark movers; now handles the new situation better
        @transform(@byrow :inew = (:k_l1.===-2) ? 1 : 0) # Mark new workers
        #@select($(Not(:j_l1)))

        groupby([:i])
        @transform(:imov_n=sum(:imov))
    end
    return df
end


function f_dgp(hp_panel::struct_panel,hp_dgp::struct_dgp;p_seed::Int64=6063700)
    
    ψ,V,f,α,ll,kk,kko,spellcount = f_dgp_panel(hp_panel,hp_dgp;p_seed=p_seed)
    jj = f_dgp_firmid(kk,kko,spellcount,hp_panel.nj_per_k;p_seed=p_seed+10)
    df_out = f_dgp_df(ll,jj,kk,α,ψ,V,f,spellcount,hp_dgp;p_seed=p_seed+11)

    return df_out
end


#===============================================================================
Functions related to calibration
=#

#===============================================================================
Functions related to AKM estimation
=#

#-------------------------------------------------------------------------------
# Connected firms
function f_connected_firms(;df_in::Union{DataFrame,Nothing}=nothing,G::Union{SimpleDiGraph{Int64},Nothing}=nothing,p_strong::Bool=false,p_quiet::Bool=true)

    if (df_in !== nothing) & (G === nothing)
        p_G = 1
    elseif (df_in === nothing) & (G !== nothing)
        p_G = 2
    else
        error("Specify only df_in or G!")
    end

    #-----------------------------------
    # Find connected firms

    if (p_G == 1)
    
    # Get edge list
    df_edge = @chain df_in begin
        @subset((:j.!=:j_l1).&(:t.!=1))
        groupby([:j_l1,:j])
        @combine(:j0=first(:j_l1),:j1=first(:j))
        @orderby(:j0,:j1)
    end
    
    # Add edge to graph
    G = @with df_edge begin
        G = SimpleDiGraph(maximum(vcat(:j0,:j1)))
        sum(mapslices(x -> add_edge!(G,x[1],x[2]),hcat(:j0,:j1);dims=2)) == length(:j0) ? nothing : println("Error: Insufficient V")
        G
    end

    end # if (p_run == 1)

    #-----------------------------------
    # Return data frame with connected firms

    # Use the largest connected set
    firm_sets = p_strong==false ? connected_components(G) : strongly_connected_components(G)
    firm_sets = sample(firm_sets,length(firm_sets), replace=false)
    firmc = firm_sets[findmax(length.(firm_sets))[2]]
    
    # First print firms not connected
    df_drop = @chain df_in begin
        @subset(@byrow !in(:j,firmc))
    end
    if nrow(df_drop) != 0
        p_quiet == false && println("Firms that are not connected:")
        p_quiet == false && println(sort(unique(df_drop.j)))
        p_quiet == false && println(string("Size of largest connected set: ",length(firmc)))
    else
        p_quiet == false && println("All firms are connected")
    end

    df_out = @chain df_in begin
        @transform(@byrow :j_connected = in(:j,firmc) ? 1 : 0)
    end;

    if p_G == 2
        return df_out
    else 
        return G,df_out
    end

end


#-------------------------------------------------------------------------------
# AKM estimate
function f_AKM(df_in;msed_tol::Float64=1e-9,p_quiet=true)

    # Initialize
    df_out = @transform(df_in,:α_hat=0.0,:ψ_hat=0.0,α_hat0=0.0,:ψ_hat0=0.0)
    mse0 = 1
    mse1 = 1
    msed = 1
    iter = 0

    while msed > msed_tol
        df_out = @chain df_out begin
            @transform(:α_hat0=:α_hat,:ψ_hat0=:ψ_hat)

            # Step 1: Update `alpha_hat` by taking the mean within `i` net of firm FE
            groupby([:i])
            @transform(:α_hat=mean(:lw.-:ψ_hat))
            # Step 2: Update `psi_hat` by taking the mean within `fid` net of worker FE
            groupby([:j])
            @transform(:ψ_hat=mean(:lw.-:α_hat))
        end
        # Check convergence
        mse1 = @with df_out begin
            mean((:lw .- :α_hat .- :ψ_hat).^2)
        end
        msed = abs(mse1 - mse0)
        mse0 = mse1
        iter = iter + 1
    end

    p_quiet == false && println(string(iter," Iterations"))

    @select!(df_out,$(Not([:α_hat0,:ψ_hat0])))
    return df_out
end


# Function that check correlation between real and estimated values
function f_cor_real(df_in,v;digits::Int64=3,est_suf::String="_hat")
    sv_hat = :($(v*est_suf))
    sv = :($v)
    println("Correlation between DGP and estimated "*v*" = ",round(cor(df_in[:,sv], df_in[:,sv_hat]),digits=digits))
end

#===============================================================================
Functions related to Sorkin paper
=#

#-------------------------------------------------------------------------------
# Aggregate flows and size over time
function f_Sorkin_flowsize(df_in)
    #-----------------------------------
    # Aggregate firm size over time (exlcuding the last t, because we cannot see moving after that)
    df_fsize = @chain df_in begin
        @subset(:t.<maximum(:t))
        @transform(:n=1)
        #@transform(@byrow :n = (:t.==maximum(:t)) ? 0 : 1)
        groupby([:k,:j])
        @combine(:n=sum(:n))
    end
    df_fsize = @orderby(df_fsize,:j)
    # Note that there could still be new firms in the last period---how to deal with that?
    # There could also be employees who move to a unconnected firm. Should we just treat them as exiting?

    #-----------------------------------
    # Aggregate flows over time

    # Bilateral flows
    df_flowbi = @chain df_in begin
        @transform(:m=1)
        groupby([:k,:j,:k_f1,:j_f1])
        @combine(:m=sum(:m))
        @subset(:j_f1.!==missing)
    end
    df_flowbi = leftjoin(df_flowbi,df_fsize,on=[:k,:j])
    @transform!(df_flowbi,:mnorm=:m./:n)

    # Total outflows
    df_flowout = @chain df_flowbi begin
        groupby([:k,:j])
        @combine(:m_out=sum(:m))
        @orderby(:j)
    end
    df_flowout = leftjoin(df_flowout,df_fsize,on=[:k,:j])
    df_flowout = @subset(df_flowout, :n.!==missing) # Drop flow to unconnected firms
    @transform!(df_flowout,:mnorm_out=:m_out./:n)
    df_flowout = @orderby(df_flowout,:j)

    # Total inflows
    df_flowin = @chain df_flowbi begin
        groupby([:k_f1,:j_f1])
        @combine(:m_in=sum(:m))
        @orderby(:j_f1)
    end
    rename!(df_flowin,"k_f1"=>"k","j_f1"=>"j")
    df_flowin = leftjoin(df_flowin,df_fsize,on=[:k,:j])
    df_flowin = @subset(df_flowin, :n.!==missing) # Drop flow to unconnected firms
    @transform!(df_flowin,:mnorm_in=:m_in./:n)
    df_flowin = @orderby(df_flowin,:j)

    return df_fsize,df_flowbi,df_flowout,df_flowin
end

#-------------------------------------------------------------------------------
# Identify f_j * e^{V_j} together based on equation 5
function f_Sorkin_fv(df_in;iter_max::Int64=1000)
    # Quality check
    @subset(df_in,:t.!=10,:j_f1.===missing,:k_f1.>0) # All j_f1 missing are from t=nt, or exits

    df_fsize,df_flowbi,df_flowout,df_flowin = f_Sorkin_flowsize(df_in)

    #-----------------------------------
    # Construct the S and M matrices in Sorkin
    # Should only apply to strongly-connected firms

    # S0^{-1} matrix
    S0i = Diagonal(df_flowout.mnorm_out)^(-1)
    # M0 matrix (normalized)
    nj = length(unique(df_flowbi.j))
    M0 = zeros(nj,nj);
    for i = 1:nj
        for j = 1:nj
            dfi = @subset(df_flowbi,:j_f1.==i,:j.==j)
            M0[i,j] = nrow(dfi) > 0 ? dfi.mnorm[1] : 0
        end
    end
    S0iM0 = S0i * M0

    #-----------------------------------
    # Iterate to find fixed points
    fv = fill(0.1,nj)
    fv0 = fill(0.1,nj)
    err = 1
    err_tol = 1e-7
    iter = 1
    # iter_max = 1000

    while (iter < iter_max) && (err > err_tol)
        fv = S0iM0 * fv0
        err = sum((fv.-fv0).^2)
        fv0 = fv
        iter = iter + 1
    end

    #-----------------------------------
    # Construct the dataframe to return

    df_out = @chain df_in begin
        groupby([:k,:j,:V,:f])
        combine(first)
        @select(:k,:j,:V,:f)
        @transform(:fv = :f.*exp.(:V))
        @orderby(:j)
        @transform(:fve = fv)
    end
    return df_out
end


#-------------------------------------------------------------------------------
# Non-linear solver (thanks to Xianglong Kong)

function f_est_ρλ(df_in)
    # Observed new workers and movers
    ρ_hat,λ_hat = @chain df_in begin
        @transform(:n=1)
        groupby([:t])
        @combine(:n=sum(:n), :nnew=sum(:inew), :nmov=sum(:imov))
        @transform(:snew=:nnew./:n,:smov=:nmov./:n)
        @subset(:t .> 1)
        mean(_.snew),mean(_.smov)
    end
    return ρ_hat,λ_hat
end

function f_Sorkin_nls(df_in)
    df_fsize,df_flowbi,df_flowout,df_flowin = f_Sorkin_flowsize(df_in);
    ρ_hat,λ_hat = f_est_ρλ(df_in)

    g = df_fsize.n
    M_in = df_flowin.m_in
    M_out = df_flowout.m_out

    nj=nrow(df_fsize)

    function f_nls_flows!(nls_res,x)
        λf = x[1:nj]
        v = x[nj+1:end]
        nls_res[1:nj] = M_out .- (1-ρ_hat) .* g .* sum(λf .* v ./ (v.*v'), dims=1)' # Total outflow
        nls_res[nj+1:end] = M_in .- (1-ρ_hat) .* λf .* sum(g' .* v ./ (v.*v'), dims=2) # Total inflow
    end

    x_init = fill(0.1,nj*2)
    x_sol = nlsolve(f_nls_flows!, x_init)

    df_out = @chain df_in begin
        groupby([:k,:j,:V,:f])
        combine(first)
        @select(:k,:j,:V,:f)
        @transform(:v = exp.(:V))
        @orderby(:j)
        @transform(:λfe = x_sol.zero[1:nj],:ve=x_sol.zero[nj+1:end])
        @transform(:Ve=log.(:ve))
    end

    return df_out
end


# println(string("Ended at ",Dates.format(now(), "yyyy-mm-dd HH:MM:SS")))
# End of Julia script