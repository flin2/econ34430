#=
ECON 34430 Topics in Labor Markets: Earnings and Employment Assignment 2
Feng Lin
cd ~/Dropbox/Chicago/ECON34430_Lamadon/HW02/
bash zbash_jl.sh "HW02_data.jl" "" ""
=#

dir_proj = "/Users/flin/Dropbox/Chicago/ECON34430_Lamadon/HW02/"
cd(dir_proj)

empty!(ARGS)
append!(ARGS, ["false"])
include("HW02_functions.jl")

hp_dgp = struct_dgp(;μ_ψ=0,μ_V=0,μ_f=0,
    σ_ψ=1,σ_V=1,σ_f=1,σ_ψV=0.75,σ_ψf=0.6,σ_Vf=0.5,
    μ_worker=0,σ_worker=1,μ_wage=0,σ_wage=1,
    λ=0.1,ρ=0.1)
hp_panel = struct_panel(10,10,10,10000,2)

@time df_raw = f_dgp(hp_panel,hp_dgp;p_seed=6063700);
df_panel = @subset(df_raw,:k.>0);

G,df_panelc = f_connected_firms(;df_in=df_panel);
G,df_panelc = f_connected_firms(;df_in=df_panel,p_strong=true);
df_panelc = @subset(df_panelc,:j_connected.==1);

# Sorkin eq 6 estimation
df_esteq6 = f_Sorkin_fv(df_panelc,iter_max=100000)
f_cor_real(df_esteq6,"fv";est_suf="e")

unique(@subset(df_panelc,:j_connected.!=1).j)



#-------------------------------------------------------------------------------
# Apply non-linear solver to the system of equations with total inflow/outflow

hp_dgp = struct_dgp(;μ_ψ=0,μ_V=0,μ_f=0,
    σ_ψ=1,σ_V=1,σ_f=1,σ_ψV=0.75,σ_ψf=0.6,σ_Vf=0.5,
    μ_worker=0,σ_worker=1,μ_wage=0,σ_wage=1,
    λ=0.1,ρ=0.1)
hp_panel = struct_panel(30,10,10,10000,5)

@time df_raw = f_dgp(hp_panel,hp_dgp);
df_panel = @subset(df_raw,:k.>0);

G,df_panelc = f_connected_firms(;df_in=df_panel)
G,df_panelc = f_connected_firms(;df_in=df_panel,p_strong=true)
df_panelc = @subset(df_panelc,:j_connected.==1)

df_in = df_panelc
df_out = f_Sorkin_nls(df_panelc,hp_panel,hp_dgp)

f_cor_real(df_out,"v";est_suf="e")
f_cor_real(df_out,"V";est_suf="e")
cor(df_out[:,:f], df_out[:,:λfe])


