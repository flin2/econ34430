# ECON 34430 Topics in Labor Markets: Earnings and Employment Assignment 3
# Feng Lin
# cd ~/Dropbox/Chicago/ECON34430_Lamadon/HW03/
# bash zbash_jl.sh "HW03_functions.jl" "" ""
# VS Code Julia 1.5.0 made some changes that I don't like...

@show ARGS

# Whether current run is for development
if ARGS == []
    sys_dev = false
else
    sys_dev = (ARGS[1]=="true")
end

if ! @isdefined dir_proj
    dir_proj = "/Users/flin/Dropbox/Chicago/ECON34430_Lamadon/HW03/" # Change to the directory where the zip file is unzipped
end
cd(dir_proj)

dir_data = "./data/"
dir_out = "./output/"

using Pkg,Dates,RCall
using Random,Distributions,StatsBase,LinearAlgebra
using DataFrames,DataFramesMeta,Chain,ShiftedArrays
using StatFiles,GLM,FixedEffectModels

#===============================================================================
Helper functions
=#

function f_setseed(p_seed::Int64=60637)
    p_seed != -1 ? Random.seed!(p_seed) : nothing
end

#=------------------------------------------------------------------------------
Normal distribution
=#

# log-normal pdf
function f_lnormpdf(y::Real,μ::Real = 0,σ::Real=1)
    pdf = -1/2 * ((y-μ)/σ)^2 - 1/2 * log(2*π) - log(σ)
    return pdf
end

function f_lnormpdf(y::Real,μ::Vector{<:Real},σ::Vector{<:Real})
    pdf = -1/2 * ((y.-μ)./σ).^2 .- 1/2 * log(2*π) .- log.(σ)
    return pdf
end

# log sum(exp)
function f_lsumexp(v)
    vmax = maximum(v)
    return log(sum(exp.(v.-vmax))) + vmax
end

#=------------------------------------------------------------------------------
Data simulation
=#

# Model parameters
function f_model_mix_new(nk::Int64=3,nt::Int64=3;
    p_seed::Int64=60637,pk::Union{Vector{<:Real},Nothing}=nothing,A::Union{Matrix{<:Real},Nothing}=nothing,S::Union{Matrix{<:Real},Nothing}=nothing)
    f_setseed(p_seed)

    # model for Y1,Y2,Y3|k 
    # Each row is a period, each column is a model
    pk = pk === nothing ? rand(Dirichlet(ones(nk))) : pk # input α shape vector
    A = A === nothing ? 3 .* (1 .+ 0.8 .* reshape(rand(Uniform(),nt*nk),nt,nk)) : A
    S = S === nothing ? ones(nt,nk) : S
    return (A=A,S=S,pk=pk,nk=nk,nt=nt) # return a named tuple
end

# Simulate panel
function f_model_mix_sim(model::NamedTuple,N::Int64=10000,sd::Real=0.8;p_seed::Int64=60637)
    f_setseed(p_seed)

    Y1 = zeros(N)
    Y2 = zeros(N)
    Y3 = zeros(N)
    K = zeros(N)

    A = model.A
    S = model.S
    pk = model.pk
    nk = model.nk

    # draw K
    K = sample(1:nk,Weights(pk),N,replace=true)

    # draw Y1, Y2, Y3
    Y1 = A[1,K] .+ S[1,K] .* rand(Normal(),N) .* sd
    Y2 = A[2,K] .+ S[2,K] .* rand(Normal(),N) .* sd
    Y3 = A[3,K] .+ S[3,K] .* rand(Normal(),N) .* sd

    dfi_wide = DataFrame(id=1:length(K),k=K,y1=Y1,y2=Y2,y3=Y3)
    dfi_long = stack(dfi_wide,[:y1,:y2,:y3], variable_name=:t, value_name=:y)
    dfi_long.t = parse.(Int64,replace.(dfi_long.t, r"y" => s""))
    
    return dfi_wide,dfi_long
end


#===============================================================================
EM algorithm
=#

#=------------------------------------------------------------------------------
E Step
=#

function f_em_e(df_in::DataFrame,pk0::Vector{<:Real},μk0::Matrix{<:Real},σk0::Matrix{<:Real},nk::Int64=3,nt::Int64=3)
    N = nrow(df_in)
    ω = zeros(N,nk) # Posterior distributions (for each individual)
    lpm = zeros(N,nk) # The numerators
    llik = 0
    lpk = log.(pk0) # Parameter from previous iteration
    lall = lpk

    for i = 1:N # Loop over individuals
        lall = lpk
        for t = 1:nt # Loop over periods, calculate ω_{ik}
            Yt = df_in[i,Symbol(string("y",t))]
            lnormt = f_lnormpdf(Yt,μk0[t,:],σk0[t,:])
            lall = lall .+ lnormt
        end
        llik = llik + f_lsumexp(lall)
        lpm[i,:] = lall'
        ω[i,:] = exp.(lall .- f_lsumexp(lall))
    end

    return ω,lpm,llik
end

#=------------------------------------------------------------------------------
M Step
=#

function f_em_m(df_in::DataFrame,ω::Matrix{<:Real},nk::Int64=3,nt::Int64=3)
    N = nrow(df_in)
    # Each column of ω corresponds to k, not t
    #-----------------------------------
    # Update pk
    pk1 = vec(mean(ω,dims=1))

    #-----------------------------------
    # Update μ and σ
    μk1 = zeros(nt,nk)
    σk1 = zeros(nt,nk)
    wgt = ω ./ sum(ω, dims=1) # Each column is a model
    yi = Matrix(select(df_in,r"^y[0-9]$")) # Each column is a period
    for k = 1:nk
        μk1[:,k] = sum(wgt[:,k] .* yi, dims=1)
        σk1[:,k] = sqrt.(sum(wgt[:,k] .* (yi .- μk1[:,k]').^2, dims=1))
    end
    
    return pk1,μk1,σk1
end

#=------------------------------------------------------------------------------
EM
=#

# Main EM function
function f_em(df_in::DataFrame,pk_init::Vector{<:Real}=vec(ones(3,1)./3),nk::Int64=3,nt::Int64=3;
    err_tol::Real=1e-9,iter_max::Int64=1000,p_seed::Int64=60637,p_reQH::Bool=false)
    
    f_setseed(p_seed)
    N = nrow(df_in)

    err = 1
    iter = 0
    llik0 = 0
    pk0 = copy(pk_init)
    μk0,σk0 = reshape(1 .+ rand(nt*nk), (nt,nk)),reshape(1 .+ rand(nt*nk), (nt,nk))
    ωn1 = zeros(N,nk)
    # Note that we need some turbulance to the initial guesses, or we will run into weird "solutions." (Why?)

    Q00 = zeros(iter_max) #τ|τ
    H00 = zeros(iter_max) #τ|τ
    Q10 = zeros(iter_max) #τ+1|τ
    H10 = zeros(iter_max) #τ+1|τ

    while err > err_tol && iter < iter_max
        # E-Step
        ω0,lpm0,llik=f_em_e(df_in,pk0,μk0,σk0,nk,nt)
        # M-Step
        pk1,μk1,σk1=f_em_m(df_in,ω0,nk,nt)
        
        err = abs(llik - llik0) # sum((pk1-pk0).^2) + sum((μk1-μk0).^2) + sum((σk1-σk0).^2)
        llik0 = llik
        pk0,μk0,σk0 = pk1,μk1,σk1
        iter = iter + 1

        Q00[iter] = sum(ω0 .* lpm0)
        H00[iter] = - sum(ω0 .* log.(ω0))
        if iter >=2
            # We are essentially calculate Q and H for the previous iteration (since we need the optimized ω given previous guess)
            Q10[iter-1] = sum(ωn1 .* lpm0)
            H10[iter-1] = - sum(ωn1 .* log.(ω0))
        end
        ωn1 = ω0
    end

    ω0,lpm0,llik=f_em_e(df_in,pk0,μk0,σk0,nk,nt)
    Q10[iter] = sum(ωn1 .* lpm0)
    H10[iter] = - sum(ωn1 .* log.(ω0))

    if p_reQH == false
        return pk0,μk0,σk0,iter
    else
        return pk0,μk0,σk0,iter,ω0,llik,Q10[1:iter],H10[1:iter],Q00[1:iter],H00[1:iter]
    end
end

#=------------------------------------------------------------------------------
Function that compares EM estimates and model parameters
=#

function f_em_comp(model,pk0,μk0,σk0)
    # Note that the EM algorithm would not identify the index (whether a type is called type one or type two really has no consequences).
    # Therefore, we need to rank the recovered types based on some criteria to compare with the true value.
    # We will simply rank them by the probability of occuring.
    pk_sort = sort(model.pk, rev=true)
    pk_ind = [findfirst(x -> x==pk,model.pk) for pk in pk_sort]
    pk0_sort = sort(pk0, rev=true)
    pk0_ind = [findfirst(x -> x==pk,pk0) for pk in pk0_sort]
    println("Model pk: ", model.pk[pk_ind])
    println("EM pk: ", pk0[pk0_ind])

    println("Model μk: ", model.A[:,pk_ind])
    println("EM μk: ", μk0[:,pk0_ind])

    println("Model σk: ", model.S[:,pk_ind])
    println("EM σk: ", σk0[:,pk0_ind])
end

#=------------------------------------------------------------------------------
Function that use EM to estimate types
=#

function f_em_est(df_in,nk::Int64,nt::Int64;iter_max::Int64=1000,p_seed::Int64=100,N::Int64=10000,sd::Real=1.0)
    df_in = copy(df_in)

    pk_init=vec(ones(nk,1)./nk)
    pk_em,μk_em,σk_em,iter_em,ω_em,llik_em,Q10_em,H10_em,Q00_em,H00_em=f_em(df_in,pk_init,nk,nt;iter_max=iter_max,p_seed=p_seed+1,p_reQH=true)
    # Assign type based on ω_em
    @transform!(df_in, :k=vec(getindex.(findmax(ω_em,dims=2)[2],2)))
    df_var = DataFrame(t=Int64[],var_y=Float64[],var_fe=Float64[])
    for i = 1:nt
        y = string("y",i)
        y_symb = Symbol(y)
        # Calculate fixed effects
        vfe = reg(df_in, term(y_symb) ~ fe(:k), save = true).fe.fe_k
        @transform!(df_in, $(Symbol(y,"_fe")) = vfe)
        # Calculate variance
        var_y = var(df_in[:,y_symb])
        var_fe = var(df_in[:,Symbol(y,"_fe")])
        push!(df_var,[i var_y var_fe])
    end
    # Calculate variance shares
    @transform!(df_var,:var_share=:var_fe./:var_y)
    
    
    #-------------------------------------------------------------------------------
    # Simulation
    
    # Define model using estimated values
    model_em = f_model_mix_new(nk,nt;p_seed=p_seed+2,pk=pk_em,A=μk_em,S=σk_em)
    # Generate simulated data
    df_pwem,df_plem = f_model_mix_sim(model_em,N,sd,p_seed=p_seed+3);
    # Reshape simulated data and prepare for plotting
    df_pld = stack(df_in[:,[:y1,:y2,:y3]],[:y1,:y2,:y3], variable_name=:t, value_name=:y)
    df_pld.t = parse.(Int64,replace.(df_pld.t, r"y" => s""))
    df_comb = vcat(@transform(df_plem[:,[:t,:y]], :data=0),@transform(df_pld[:,[:t,:y]], :data=1))
    # Make plot
    gg_out = ggplot(df_comb, aes(x=:y, color=R"factor($(df_comb.data))")) + 
        geom_density() + facet_wrap(R"~ t") + 
        scale_colour_discrete(name="",labels=["Simulated","Data"]) +
        theme_bw()
    
    return df_var,gg_out
end

# End of jl script