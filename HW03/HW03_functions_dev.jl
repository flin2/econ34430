
dir_proj = "/Users/flin/Dropbox/Chicago/ECON34430_Lamadon/HW03/"
cd(dir_proj)
include("HW03_functions.jl")
@rlibrary ggplot2

nk=3
nt=3
sd=0.5
N=10000

err_tol=1e-9
iter_max=1000
pk_init=[1/3,1/3,1/3]

model = f_model_mix_new(nk,nt;p_seed=1001);
df_pw,df_pl = f_model_mix_sim(model,N,sd,p_seed=1002);
ggplot(df_pl,aes(x=:y,group=:k,fill=R"factor($df_pl$k)")) + geom_density() + facet_grid(R"~t") + theme_bw()


pk_em,μk_em,σk_em,iter_em=f_em(df_pw;iter_max=1000,p_seed=100)
pk_em,μk_em,σk_em,iter_em,ω_em,llik_em,Q10_em,H10_em,Q00_em,H00_em=f_em(df_pw;iter_max=1000,p_seed=100,p_reQH=true)
pk_em
model.pk
μk_em
model.A
Q_em
H_em

f_em_comp(model,pk_em,μk_em,σk_em)

df_QH = DataFrame(iter=1:iter_em,Q10=Q10_em,H10=H10_em,Q00=Q00_em,H00=H00_em)
@transform!(df_QH, :Qd=:Q10-:Q00, :Hd=:H10-:H00)
ggplot(@subset(df_QH,:iter.>=2)) + 
    geom_line(aes(x=:iter,y=:Qd,color="Q")) + geom_line(aes(x=:iter,y=:Hd,color="H")) + 
    scale_color_manual(name = "Colors",values = R"c('Q' = 'navy', 'H' = 'maroon')") + theme_bw()


#===============================================================================
Empirical
=#

#=------------------------------------------------------------------------------
Prepare data
=#

using StatFiles,GLM,FixedEffectModels
df_raw = DataFrame(load(dir_data*"data4estimation.dta"))
@select!(df_raw,:person,:year,:marit,:state_st,:log_y) # Keep variables that matters
df_raw = @transform df_raw begin
    :person = convert.(Int64,:person)
    :year = convert.(Int64,:year)
    :marit = convert.(Int64,:marit)
    :log_y = convert.(Union{Float64,Missing},:log_y)
    :state_st = convert.(String,:state_st)
end
df_raw[:,:lyr] = reg(df_raw, @formula(log_y ~ fe(year) + fe(state_st)), save = true).residuals;
df_out = @chain df_raw begin
    @orderby(:person,:year)
    groupby(:person)
    @transform(:lyr_l1=lag(:lyr,1),:lyr_l2=lag(:lyr,2))
    @subset(:lyr.*:lyr_l1.*:lyr_l2.!==missing)
    @select(:person,:lyr_l2,:lyr_l1,:lyr)
end
rename!(df_out,:person => :id,:lyr_l2 => :y1,:lyr_l1 => :y2,:lyr => :y3)

#=------------------------------------------------------------------------------
EM Estimates
=#

nk = 3
nt = 3
iter_max = 1000
p_seed=100
N = 10000
sd = 0.5

nk=4
df_in = copy(df_out)

function f_em_est(df_in,nk,nt;iter_max::Int64=1000,p_seed::Int64=100,N::Int64=10000,sd::Real=1.0)
df_in = copy(df_in)
pk_init=vec(ones(nk,1)./nk)
pk_em,μk_em,σk_em,iter_em,ω_em,Q10_em,H10_em,Q00_em,H00_em=f_em(df_in,pk_init,nk,nt;iter_max=iter_max,p_seed=p_seed+1,p_reQH=true)
# Assign type based on ω_em
@transform!(df_in, :k=vec(getindex.(findmax(ω_em,dims=2)[2],2)))
df_var = DataFrame(t=Int64[],var_y=Float64[],var_fe=Float64[])
for i = 1:nt
    y = string("y",i)
    y_symb = Symbol(y)
    # Calculate fixed effects
    vfe = reg(df_in, term(y_symb) ~ fe(:k), save = true).fe.fe_k
    @transform!(df_in, $(Symbol(y,"_fe")) = vfe)
    # Calculate variance
    var_y = var(df_in[:,y_symb])
    var_fe = var(df_in[:,Symbol(y,"_fe")])
    push!(df_var,[i var_y var_fe])
end
# Calculate variance shares
@transform!(df_var,:var_share=:var_fe./:var_y)


#-------------------------------------------------------------------------------
# Simulation

# Define model using estimated values
model_em = f_model_mix_new(nk,nt;p_seed=p_seed+2,pk=pk_em,A=μk_em,S=σk_em)
# Generate simulated data
df_pwem,df_plem = f_model_mix_sim(model_em,N,sd,p_seed=p_seed+3);
# Reshape simulated data and prepare for plotting
df_pld = stack(df_in[:,[:y1,:y2,:y3]],[:y1,:y2,:y3], variable_name=:t, value_name=:y)
df_pld.t = parse.(Int64,replace.(df_pld.t, r"y" => s""))
df_comb = vcat(@transform(df_plem[:,[:t,:y]], :data=0),@transform(df_pld[:,[:t,:y]], :data=1))
# Make plot
gg_out = ggplot(df_comb, aes(x=:y, color=R"factor($(df_comb.data))")) + 
    geom_density() + facet_wrap(R"~ t") + 
    scale_colour_discrete(name="",labels=["Simulated","Data"]) +
    theme_bw()

return df_var,gg_out
end

f_em_est(df_out,3,3)

#=------------------------------------------------------------------------------
Auto-correlation
=#

df_data2 = @chain df_raw begin
    @orderby(:person,:year)
    groupby(:person)
    @transform(:lyr_l1=lag(:lyr,1),:lyr_l2=lag(:lyr,2),:lyr_l3=lag(:lyr,3))
    @subset(:lyr.*:lyr_l1.*:lyr_l2.*:lyr_l3.!==missing)
    @select(:person,:lyr_l3,:lyr_l2,:lyr_l1,:lyr)
end
rename!(df_data2,:person => :id)

df_in = df_data2

function f_data_ρ(df_in::DataFrame,ρ::Real)
    df_out = @chain df_in begin
        @transform begin
            :y1 = :lyr_l2 - ρ * :lyr_l3
            :y2 = :lyr_l1 - ρ * :lyr_l2
            :y3 = :lyr - ρ * :lyr_l1
        end
    end
    return df_out
end

df_dataρ = f_data_ρ(df_data2,0.6)
f_em_est(df_dataρ,3,3)


#---------------------------------------
# Search over grid
pk_init=vec(ones(nk,1)./nk)

llik_grid = zeros(501)

for i=0:500
    df_dataρ = f_data_ρ(df_data2,1/500*i)
    pk_em,μk_em,σk_em,iter_em,ω_em,llik_em,Q10_em,H10_em,Q00_em,H00_em=f_em(df_dataρ,pk_init,nk,nt;iter_max=iter_max,p_seed=p_seed+1,p_reQH=true)
    llik_grid[i+1] = llik_em
end